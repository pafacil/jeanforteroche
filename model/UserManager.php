<?php

namespace JeanForteroche\Model;

require_once("model/Manager.php");

class UserManager extends Manager 
{
	public function getUser($mail) {
		$db = $this -> connectDb();
		$user = $db -> prepare('SELECT * FROM user WHERE mail = ?');
		$user -> execute(array($mail));
		return $user;
	}	

	public function checkPassword($mail, $password) {
		$db = $this -> connectDb();
		$req_password = $db -> prepare('SELECT password FROM user WHERE mail = ?');	
	    $req_password -> execute(array($mail));	
		$hashed_password = $req_password -> fetch();

		$isValidPassword = (password_verify($password, $hashed_password['0'])) ? true : false;

		// if (password_verify($password, $hashed_password['0'])) {
		// 	$isValidPassword = true;
		// } else {
		// 	$isValidPassword = false;
		// }

		return $isValidPassword;			
	}	
}
