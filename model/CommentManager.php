<?php

namespace JeanForteroche\Model;

require_once("model/Manager.php");

class CommentManager extends Manager
{
	public function getComment($comment_id) {
		$db = $this -> connectDb();
		$req_comment = $db -> prepare('SELECT * FROM comment WHERE id = ? ');	
	    $req_comment -> execute(array($comment_id));	
		$res_comment = $req_comment -> fetch();
		return $res_comment;	
	}

	public function getComments($post_id,$order) {
		$db = $this -> connectDb();
		if ($post_id == '') {
		$req_comments = $db -> prepare('SELECT c.id, p.title AS title, c.author, c.content, DATE_FORMAT(c.creation_date, \'%d/%m/%Y à %Hh%i\') AS date_creation, c.warning, c.status FROM comment c INNER JOIN post p ON p.id = c.post_id WHERE status = "1" AND warning > 0 ORDER BY status, warning DESC');		
		}
		else if ($order == "warning") {
		$req_comments = $db -> prepare('SELECT id, author, content, DATE_FORMAT(creation_date, \'%d/%m/%Y à %Hh%i\') AS date_creation, warning, status FROM comment WHERE post_id = ? AND status != "0" ORDER BY status, warning DESC LIMIT 0,10');
		} else {
		$req_comments = $db -> prepare('SELECT id, author, content, DATE_FORMAT(creation_date, \'%d/%m/%Y à %Hh%i\') AS date_creation FROM comment WHERE post_id = ? AND status != "0" ORDER BY id LIMIT 0,10');	
		}
		
		$req_comments -> execute(array($post_id)); 
		return $req_comments;
	}

	public function getNbCommentPerPost($post_id) {
		$db = $this -> connectDb();
		$req_nb_comments = $db -> prepare('SELECT COUNT(id) FROM comment WHERE post_id = ? AND status != "0"');
		$req_nb_comments -> execute(array($post_id));
		$res_nb_comments = $req_nb_comments -> fetch();
		$nb_comments = ($res_nb_comments['0']) ?  $res_nb_comments['0'] : 0;	
		return $nb_comments;
	}

	public function insertComment($post_id, $name, $mail, $message) {
		$db = $this -> connectDb();
		$req_insert_comment = $db -> prepare('INSERT INTO comment(post_id, author, mail, content, creation_date, warning, status) VALUES (?,?,?,?,NOW(),0,1)');
		$req_insert_comment -> execute(array($post_id, $name, $mail, $message));
		$res_insert_comment = $req_insert_comment->fetch();
		$req_insert_comment -> closeCursor();
	}

	public function insertCommentWarning($comment_id) {
		$db = $this -> connectDb();
		$req_insert_warning = $db -> prepare('UPDATE comment SET warning = warning + 1 WHERE id = ?');
		$req_insert_warning -> execute(array($comment_id));
		$res_insert_warning = $req_insert_warning -> fetch();
		$req_insert_warning -> closeCursor();
	}

	public function deleteComments($post_id) {
		$db = $this -> connectDb();
		$req_delete_comments = $db -> prepare('DELETE FROM comment WHERE post_id = ?');
		$req_delete_comments -> execute(array($post_id));
		$res_delete_comments = $req_delete_comments -> fetch();
		$req_delete_comments -> closeCursor();
	}

	public function getNbWarningPerComment($comment_id) {
		$db = $this -> connectDb();
		$req_nb_warning = $db -> prepare('SELECT warning FROM comment WHERE post_id = ?');
		$req_nb_warning -> execute(array($comment_id));	
		$res_nb_warning = $req_nb_warning -> fetch();
		$nb_warning = ($res_nb_warning['0']) ?  $res_nb_warning['0'] : 0;	
		return $nb_warning;
	}

	public function actionComment($comment_id, $action) {
		$db = $this -> connectDb();
		if ($action == "ban") {$status = 0;}
		if ($action == "checked") {$status = 2;}
		$req_ban_comment = $db -> prepare('UPDATE comment SET status = ? WHERE id = ?');
		$req_ban_comment -> execute(array($status,$comment_id));
		$res_ban_comment = $req_ban_comment -> fetch();
		$req_ban_comment -> closeCursor();
	}	
}