<?php

namespace JeanForteroche\Model;

require_once("model/Manager.php");

class PostManager extends Manager
{
	public function getPost($post_id) {
		$db = $this -> connectDb();
		$req_post = $db -> prepare('SELECT * FROM post WHERE id = ? ');	
	    $req_post -> execute(array($post_id));
	  	$res_post = $req_post -> fetch();
	  	return $res_post;
	}

	public function getLastPost() {
		$db = $this -> connectDb();
		$req_last_post = $db -> query('SELECT id, title, content, creation_date FROM post ORDER BY id DESC LIMIT 0,1');
		$res_last_post = $req_last_post -> fetch();
		return $res_last_post;
	}

	public function getPostsList() {
		$db = $this -> connectDb();
		$req_post_list = $db -> query('SELECT id, title FROM post ORDER BY id');
		return $req_post_list;
	}

	public function getRankedPost() {
		$db = $this -> connectDb();
		$ranking = [];
		$ordre = 1;
		$req_ranking = $db -> query('SELECT id FROM post ORDER BY id');
		while ($res_ranking = $req_ranking -> fetch()) {
			$ranking[$ordre] = $res_ranking['0'];
			$ordre++;
		}
		return $ranking;
	}	

	public function insertPost($title, $post) {
		$db = $this -> connectDb();
		$req_insert_post = $db -> prepare('INSERT INTO post(title, content, creation_date) VALUES (?,?,NOW())');
		$req_insert_post -> execute(array($title, $post));
		$res_insert_post = $req_insert_post->fetch();
		// $req_insert_post -> closeCursor();
	}	

	public function deletePost($post_id) {
		$db = $this -> connectDb();
		$req_delete_post = $db -> prepare('DELETE FROM post WHERE id = ? ');
	    $isDeleted = $req_delete_post -> execute(array($post_id));
	  	return $isDeleted;
	}	

	public function updatePost($post_id, $title, $post) {
		$db = $this -> connectDb();
		$req_update_post = $db->prepare('UPDATE post SET title = ?, content = ?, creation_date = NOW() WHERE id = ?');
	    $isUpdated = $req_update_post -> execute(array($title, $post, $post_id));
	  	return $isUpdated;
	}		
}
