<?php 
require('controller/controller.php');
require('controller/commentController.php');
require('controller/postController.php');

try {
	session_start();	

	// Logout, back to the blog
	if(isset($_GET['action']) && $_GET['action'] == "deconnexion") {
		unset($_SESSION['activeSession']);
	}

	$post_id = (isset($_GET['post'])) ? htmlspecialchars((int) $_GET['post']) : 0;	
	$comment_id = (isset($_GET['comment'])) ? htmlspecialchars((int) $_GET['comment']) : 0;

	// Public blog
	if(isset($_GET['action']) && $_GET['action'] == "commentWarning") {
		commentWarning($comment_id);
	}

	else if(isset($_GET['action']) && $_GET['action'] == "insertComment") {
		addComment();
	}

	// Administration work
	else if (isset($_GET['access']) && $_GET['access'] == "adminblog") {
		// Login once 
		if (isset($_GET['action']) && $_GET['action'] == "login") {
			login();
		// First login screen	
		} else if (!isset($_SESSION['activeSession']) OR (isset($_GET['action']) && $_GET['action'] == "incorrectCredentials")) {
			loginScreen();
		} else {
			$edit_id = (isset($_GET['edit'])) ? htmlspecialchars((int) $_GET['edit']) : 0;
print_r($_SESSION);

			// Action : comments moderation
			if(isset($_GET['action']) && ($_GET['action'] == "commentBanned" || $_GET['action'] == "commentChecked")) {
				commentAction($comment_id);
			}	
			// Action : add a post
			else if(isset($_GET['action']) && $_GET['action'] == "addPost") {
				addPost();
			}
			// Screen : create a new post
			else if(isset($_GET['action']) && $_GET['action'] == "create") {
				createPost();
			}	
			// Screen : edit an existing post
			else if(isset($_GET['action']) && $_GET['action'] == "edit") {
				editPost($post_id);
			}
			// Action : update an existing post			
			else if(isset($_GET['action']) && $_GET['action'] == "updatePost") {
				updatePost($post_id);
			}	
			// Action : delete an existing post					
			else if(isset($_GET['action']) && $_GET['action'] == "delete") {
				deletePost($post_id);
			}	
			// Action : add a post
			else if(isset($_GET['action']) && $_GET['action'] == "moderation") {
				// Action : moderate a comment
				if ($comment_id > 0) {
					commentAction($comment_id);
				}
				// Screen : moderate comments
				moderateComments();
			}	
			else {
			adminBlog($post_id);	
			}
		}
	} 

	else {
		unset($_SESSION['activeSession']);
		publicBlog($post_id);
	}
}
catch(Exception $e) {
  $errorMsg = $e->getMessage();
  require('view/errorView.php');  
}
