// To Navigate among chapters with Arrows / Keys
var prev = document.getElementById("leftArrow");
var next = document.getElementById("rightArrow");
var moderation = document.getElementById("moderation");
var newChapter = document.getElementById("newChapter");
var editChapter = document.getElementById("editChapter");
var readChapter = document.getElementById("readChapter");
var deleteChapter = document.getElementById("deleteChapter");
var myComments = document.getElementById("myComments");
var theComments = document.getElementById("theComments");
var formComment = document.getElementById("formComment");
var down = document.getElementById("down");
var down_bis = document.getElementById("down_bis");
var activeFormComment = document.getElementById("activeFormComment");
var footerLogo = document.getElementById("footerLogo");
var identity = document.getElementById("identity");
var closeIdentity = document.getElementById("closeIdentity");
var headerLogo = document.getElementById("headerLogo");
var bibliography = document.getElementById("bibliography");
var closeBibliography = document.getElementById("closeBibliography");
var helpRequired = document.getElementById("helpRequired");
var help = document.getElementById("help");
var closeHelp = document.getElementById("closeHelp");
var alert = document.getElementById("alert");
var go = document.getElementById("go");
var all = document.getElementById('all'); 
var snow = document.getElementById('snow'); 


if (typeof go !== 'undefined' && go !== null) {
   go.addEventListener("click", function(e){
      window.location.href = 'index.php';
   });
}

if (typeof all !== 'undefined' && all  !== null) {
   adjustHeight();
}

window.addEventListener('resize', adjustHeight);

function adjustHeight() {
   var allHeight; 

   if(all.offsetHeight) {
      allHeight = all.offsetHeight;
   } else if(all.style.pixelHeight) {
      allHeight = all.style.pixelHeight;
   }
   var diffHeight = allHeight - window.innerHeight;

   if (diffHeight > 0) {
      snow.style.bottom = '-' + diffHeight + 'px';
   }   
}

formComment.addEventListener("click", function(e){
   activeFormComment.classList.toggle("hidden");
   theComments.classList.add("hidden");
   down_bis.classList.toggle("rotate");   
   if (down.classList.contains('rotate')) {
      down.classList.toggle("rotate");      
   }});

myComments.addEventListener("click", function(e){
   theComments.classList.toggle("hidden");
   activeFormComment.classList.add("hidden"); 
   down.classList.toggle("rotate");   
   if (down_bis.classList.contains('rotate')) {
      down_bis.classList.toggle("rotate");      
   }
});

footerLogo.addEventListener("click", function(e){
   identity.classList.toggle("panelOn");
});

closeIdentity.addEventListener("click", function(e) {
   identity.classList.toggle("panelOn");
});

headerLogo.addEventListener("click", function(e){
   bibliography.classList.toggle("panelOn");
});

closeBibliography.addEventListener("click", function(e) {
   bibliography.classList.toggle("panelOn");
});

helpRequired.addEventListener("click", function(e){
   help.classList.toggle("panelOn");
});

closeHelp.addEventListener("click", function(e) {
   help.classList.toggle("panelOn");
});

document.addEventListener("keydown",function(e){
   var key = e.which||e.keyCode;
 
   switch(key){
      // Left arrow to go to the previous chapter
      case 37:
         prev.click();
      break;
      // Right arrow to go to the next chapter
      case 39:
         next.click();
      break;    
   }
});

// To navigate entering chapter numbers
var keySeq = '';
document.addEventListener("keypress",function(e){
   var keycode = e.which||e.keyCode;
   var key = String.fromCharCode(keycode);
   if (key >= 0 && key <= 9) {
      if (keySeq === '') {keySeq = key;} else {keySeq = keySeq + key;}
   }
   setTimeout(goToChapter,500);
});

function goToChapter() {
   if (document.getElementById(keySeq)) {
      document.getElementById(keySeq).click();
   }
   keySeq = '';
}

// Arrow to top
$(document).ready(function(){  
   //Check to see if the window is top if not then display button
   $(window).scroll(function(){
      if ($(this).scrollTop() > 200) {
         $('.scrollBtn').addClass("On");
      } else {
         $('.scrollBtn').removeClass("On");
      }
   });
   
   //Click event to scroll to top
   $('.scrollBtn').click(function(){
      $('html, body').animate({scrollTop : 0},800);
      return false;
   });   
});
