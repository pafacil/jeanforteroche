// To Navigate among chapters with Arrows / Keys
var prev = document.getElementById("leftArrow");
var next = document.getElementById("rightArrow");
var moderation = document.getElementById("moderation");
var newChapter = document.getElementById("newChapter");
var editChapter = document.getElementById("editChapter");
var readChapter = document.getElementById("readChapter");
var deleteChapter = document.getElementById("deleteChapter");
var theComments = document.getElementById("theComments");
var activeComments = document.getElementById("activeComments");
var down = document.getElementById("down");
var helpRequired = document.getElementById("helpRequired");
var help = document.getElementById("help");
var closeHelp = document.getElementById("closeHelp");

// Comments section opening
if (typeof theComments !== 'undefined' && theComments !== null) {
   theComments.addEventListener("click", function(e){
   activeComments.classList.toggle("hidden");
   down.classList.toggle("rotate");
   });
}

helpRequired.addEventListener("click", function(e){
   help.classList.toggle("panelOn");
});

closeHelp.addEventListener("click", function(e) {
   help.classList.toggle("panelOn");
});

// Keyboard interaction
document.addEventListener("keydown",function(e){
   var key = e.which||e.keyCode;
 
   switch(key){
      // Left arrow to go to the previous chapter
      case 37:
         prev.click();
      break;
      // Right arrow to go to the next chapter
      case 39:
         next.click();
      break;
      // Up Arrow - for "New" to go to the post creation page - admin only
      case 38:
         newChapter.click();
      break;       
      // Down Arrow Key to go to the "read" page - admin only
      case 40:
         readChapter.click();
      break;       
      // M Key to go to the "comments to moderate" page - admin only
      case 77:
         moderation.click();
      break; 
      // E Key to go to the "edit" page - admin only
      case 69:
         editChapter.click();
      break;
      // S Key to delete the chapter - admin only
      case 83:
         deleteChapter.click();
      break;    
      // C Key to open / close the comments section - admin only
      case 67:
         theComments.click();
      break;          
   }
});

// To navigate entering chapter numbers
var keySeq = '';
document.addEventListener("keypress",function(e){
   var keycode = e.which||e.keyCode;
   var key = String.fromCharCode(keycode);
   if (key >= 0 && key <= 9) {
      if (keySeq === '') {keySeq = key;} else {keySeq = keySeq + key;}
   }
   setTimeout(goToChapter,500);
});

function goToChapter() {
   if (document.getElementById(keySeq)) {
      document.getElementById(keySeq).click();
   }
   keySeq = '';
}


// Arrow to top
$(document).ready(function(){  
   //Check to see if the window is top if not then display button
   $(window).scroll(function(){
      if ($(this).scrollTop() > 200) {
         $('.scrollBtn').fadeIn("slow",'');
      } else {
         $('.scrollBtn').fadeOut("slow",'');
      }
   });
   
   //Click event to scroll to top
   $('.scrollBtn').click(function(){
      $('html, body').animate({scrollTop : 0},800);
      return false;
   });   
});