<?php
$title = 'Billet simple pour l\'Alaska'; 
$head_options = ''; 

ob_start(); 
	if (isset($msgTitle)) { ?>
		<section id="alert">
			<div class="overlay">
				<div id="message">
					<h1>
						<?= $msgTitle ?>        	
						<div id="closeMsg" class="fas fa-times-circle"></div>
					</h1>
					<?= $msgContent ?>
				</div>
			</div>
		</section> 
	<?php } ?>
    <section id="book">
	  	<div id="chapter">
	  		<div id="chapterTitle">
				<?php if ($prev_id != 0) { ?>
					<div id="prev"><a href="index.php?post=<?= $prev_id ?>" id="leftArrow" class="fas fa-angle-left"></a></div>
				<?php } ?>
				<h3 id="title">
                <?= htmlspecialchars($res_post['title']); ?>
				</h3>
				<?php if ($next_id != 0) { ?>
					<div id ="next"><a href="index.php?post=<?= $next_id ?>" id="rightArrow" class="fas fa-angle-right"></a></div>
				<?php } ?>
            </div>			

			<p id="chapterContent"><?= html_entity_decode($res_post['content']); ?></p>
		</div>	

		<div id="comments">
			<div id="myComments">Commentaires <?= '('.$nb_comments.')'; ?> 
			<span id="down" <?php if ($nb_comments > 0) { ?>class="fas fa-caret-down"><?php } ?></span>			
			</div><!--
			--><div id="formComment">Mon commentaire 
				<span id="down_bis" class="fas fa-caret-down"></span> 
			</div>

	        <div id="theComments" class="hidden">
				<?php 
				while ($res_comments = $req_comments -> fetch()) { 
				  	echo '<div class="myComment">
			  			<p><strong>'.htmlspecialchars($res_comments['author']).'</strong> a posté le '.$res_comments['date_creation'].'</p>
			  			<p class=myCommentContent>'.nl2br(htmlspecialchars($res_comments['content'])).'</p>
			  			<a href="index.php?action=commentWarning&amp;comment='.htmlspecialchars($res_comments['id']).'"  class="warningComment">Signaler</a>
			  			</div> ';
				} ?>
			</div>

			<form action="index.php?action=insertComment" method="post" id="activeFormComment" class="hidden">
				<input type="text" id="name" name="name" placeholder="...mon pseudo..." required><br>
				<input type="email" id="mail" name="mail" placeholder="...mon mail..." required><br>
				<textarea name="message" rows="3" placeholder="...mon message..." required></textarea><br>
				<input type="hidden" name="post_id" value="<?= $post_id; ?>">
				<input type="submit" value="Envoyer">
			</form>				
		</div>
	</section>

	<aside id="chapters_toc">
		<h3>Chapitres</h3>
		<ul>
		  <?php 
		  $post_nb = 1;
		  while ($res_post_list = $req_posts_list -> fetch())
		  	{
		  	if ($post_id == htmlspecialchars($res_post_list['id'])) { ?>
				<li class="chapterNumber"><strong><?= $post_nb.' - '.htmlspecialchars($res_post_list['title']); ?></strong></li>
		  	<?php }
		  	else { ?>
				<li class="chapterNumber"><a href="index.php?post=<?= htmlspecialchars($res_post_list['id']);?>" id="<?= $post_nb ?>"> <?= $post_nb.' - '.htmlspecialchars($res_post_list['title']); ?></a></li>
		  	<?php }
		  	$post_nb++;
		  }	?>
		 </ul>
	</aside>	
<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>