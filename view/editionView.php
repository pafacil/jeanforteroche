<?php
$title = 'Billet simple pour l\'Alaska'; 

ob_start(); ?>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=af0d9ejcr64vmi9suqa2tf5c2gdr0wx765n2roiupaxq58p6"></script>
<script>tinymce.init({selector: 'textarea', entity_encoding: "raw", width: "90%"});</script>
<?php $head_options = ob_get_clean();  

ob_start(); ?>
    <section id="book">
    	<div id="chapters">
		  	<div class="chapter">
				<form action="<?= $refAction ?>" method="post" id="formEdit">
					<div id="chapterTitle">
							<div id="prev"><a href="<?= $refPrev ?>" id="<?php if ($prev_id == 0) {echo 'invisibleArrow';} else {echo 'leftArrow';} ?>" class="fas fa-angle-left"></a></div>
			                <input type="text" id="editTitle" name="title" <?= $refTitle ?> required>
							<div id ="next"><a href="<?= $refNext ?>" id="<?php if ($next_id == 0) {echo 'invisibleArrow';} else {echo 'rightArrow';} ?>" class="fas fa-angle-right"></a></div>
		            </div>
					
					<textarea name="post" rows="10"><?= $refContent ?></textarea><br>
					<input type="submit" class="submit" value="Valider">
				</form>			  		
			</div>
		</div>
	</section>
<?php $content = ob_get_clean(); 

require('template_admin.php'); ?>