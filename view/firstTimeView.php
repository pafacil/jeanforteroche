<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
	    <title>Billet simple pour l'Alaska</title>
	    <link href="public/css/style_first.css" rel="stylesheet" /> 
	    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"> 
	</head>

	<body class='firstTime' id="all">

		<div class="snow" id="snow">
		  <div class="snow__layer"><div class="snow__fall"></div></div>
		  <div class="snow__layer"><div class="snow__fall"></div></div>
		  <div class="snow__layer">
		    <div class="snow__fall"></div>
		    <div class="snow__fall"></div>
		    <div class="snow__fall"></div>
		  </div>
		  <div class="snow__layer"><div class="snow__fall"></div></div>
		</div>	
	
 		<section id="alert">
			<div class="info">
				<h1>Billet <span class="special">simple</span> pour l'Alaska
				</h1>
				<a id="go" href="index.php"><img src="public/images/open.png" alt="Panneau Ouvert"></a>
			</div>
			<div class="info">
				<h1>Un roman en ligne de Jean Forteroche</h1>
				<img src="public/images/jf.png" alt="Mon portrait">
				<p>Vous voyez ce message parce que vous êtes ici pour la première fois. Mais il se pourrait bien que ce ne soit qu'une première visite d'un long périple. Sachez que ce blog est en fait mon dernier livre !</p>
				<h2>Un livre qui s'écrit au fil des semaines</h2>
				<p>Chaque semaine, je mets en ligne un nouveau chapitre. Vous arrivez automatiquement sur le dernier chapitre, prêt à découvrir les dernières aventures de Noam, le héros de cette saga.</p>
			</div>
			<div class="info">	
				<h1>Un livre qu'on lit comme une série</h1>
				<img src="public/images/book.png" alt="Un livre ouvert">		
				<p>Alors, partez du premier chapitre, lisez l'existant puis revenez chaque semaine pour accompagner Noam dans ses aventures. </p>
				<h2>Régalez-vous !</h2>
				<p>Vous pouvez naviguer à travers les chapitres par les flèches, grâce au sommaire ou directement en tapant au clavier le numéro du chapitre.</p> 	
			</div>
			
		</section>		

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	    <script src="public/js/main.js"></script>                                            
	</body>
</html>