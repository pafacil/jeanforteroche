<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
	    <title><?= $title ?></title>
	    <link href="public/css/style.css" rel="stylesheet" /> 
	    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"> 
	    <?= $head_options ?>
	</head>
	<body>

		<header>
			<div id="headerLogo">
					<img src="public/images/book2.png" alt="Logo livre" title="Ma bibliographie">
					<img src="public/images/book.png" alt="Logo livre" title="Ma bibliographie">
			</div>
			<div id="headerContent">
				<h1>Billet <span class="special">simple</span> pour l'Alaska</h1>
				<h2>Un roman de Jean Forteroche</h2>
				<form action="index.php?access=adminblog" method="post" enctype="multipart/form-data" id="connexion">
					<input type="submit" class="connexion" value="Connexion"/>	
				</form>
				<div id="helpRequired">Aide</div>
			</div>
		</header>

		<span class="scrollBtn"></span>

	    <?= $content ?>

        <article id="identity">
        	<div id="closeIdentity" class="fas fa-times-circle"></div>
        	<h1>Jean Forteroche</h1>
        	<img src="public/images/jf.png" alt="Jean Forteroche">
        	<p>J'ai découvert la passion de la lecture très jeune. Et c'est très naturellement que je me suis porté vers des études de littérature.</p>
        	<h2>Profession Écrivain</h2><hr>
        	<p>Mes premiers romans remontent à loin désormais. Certains ont été boudés par la critique, d'autres encensés et récompensés, sans que je ne sache trop quelle est la différence entre les deux. Mais l'important, c'est que le public soit réceptif à mon imaginaire.</p>
        	<h2>Mes autres projets</h2><hr>
        	<p>À mes heures perdues, j'aime à pratiquer le cerf-volant de compétition et la collection d'images Panini années 85-88.</p>
        </article>

        <article id="bibliography">
        	<div id="closeBibliography" class="fas fa-times-circle"></div>
        	<h1>Ma bibliographie</h1>
        	<img src="public/images/bibliography.png" alt="Ma bibliographie">
        	<p>Un billet simple en Alaska signe un changement de cap, puisqu'il s'agit de mon premier polar. </p>
        	<h2>Martine à la plage</h2><hr>
        	<p>Un mélodrame acclamé unanimement par la presse et le public. Mon plus grand succès d'estime.</p>        	
        	<h2>Martine fait une quiche aux lardons</h2><hr>
        	<p>Une expérience renversante riche en rebondissements.</p>
        	<h2>Martine a un ordinateur</h2><hr>
        	<p>Un roman d'anticipation qui nous parle de notre potentiel futur.</p>
        </article>

        <article id="help">
        	<div id="closeHelp" class="fas fa-times-circle"></div>
        	<h1>Aide</h1>
        	<img src="public/images/help.png" alt="Aide">
        	<h2>Navigation</h2><hr>
        	<p>Pour naviguer à travers les chapitres, vous pouvez utiliser les flèches ou l'index.<br/> Vous pouvez également naviguer au clavier (flèches droite et gauche, ainsi que directement en tapant le numéro d'un chapitre).</p>        	
        	<h2>Commentaires</h2><hr>
        	<p>Sous chaque chapitre du livre-blog, vous trouverez une section commentaires. Vous pouvez ajouter un commentaire en cliquant sur l'onglet "Mon commentaire" et en remplissant les champs.</p>
        	<p>Si vous voyez un commentaire insultant ou hors de propos, n'hésitez pas à le signaler, je le vérifierai ensuite.</p>
        </article>        

		<footer>
			<div id="footerContent">
	            <strong>© Copyright - Tous droits réservés - 2018</strong>
	            <div id="footerLogo">
					<img src="public/images/pen2.png" alt="Logo encrier" title="Qui suis-je ?">
					<img src="public/images/pen.png" alt="Logo encrier" title="Qui suis-je ?">
	            </div>	            
	        </div>
		</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	    <script src="public/js/main.js"></script>                                            
	</body>
</html>