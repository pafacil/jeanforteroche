<?php
$title = 'Administration du blog - Identification'; 
$head_options = ''; 
$header = '';

ob_start(); ?>
	<div class="overlay">
		<div id='login'>
            <h1>ADMINISTRATION DU BLOG</h1>
	        <form action="index.php?access=adminblog&amp;action=login" method="post">
				<label for="userMail">Utilisateur * </label><input type="email" name="userMail" placeholder="Votre mail..." autofocus required/><br>
	            <label for="password">Mot de passe * </label><input type="password" name="password" required/>
	            <?php echo $warningMessage; ?>
				<input type="submit" name="submit" class="submit" value="Valider"/>  
			</form>  
	        <a href="index.php">Retour au blog</a>
		</div>	
	</div>
	<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>