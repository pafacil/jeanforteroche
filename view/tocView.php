		<aside id="chapters_toc">
			<h3>Chapitres</h3>
			<ul>
			<?php if (isset($new_chapter_to_create)) { ?>
			  <li><a href="index.php?access=adminblog&amp;action=create" id="newChapter" title="Nouveau (Flèche Bas)">Nouveau Chapitre</li>	
			  <?php }
			  $post_nb = 1;
			  while ($res_post_list = $req_posts_list -> fetch())
			  	{
			  	if ($post_id == htmlspecialchars($res_post_list['id'])) { ?>
					<li>
						<div id="tocEntry">
							<a href="index.php?access=adminblog&amp;post=<?= htmlspecialchars($res_post_list['id']);?>" class="icon" <?= $refRead?> >
								<img src="public/images/read.png" alt="Icône Lire">
							</a>
							<a href="index.php?access=adminblog&amp;action=edit&amp;post=<?= htmlspecialchars($res_post_list['id']);?>" class="icon"  <?= $refEdit?> >
								<img src="public/images/edit.png" alt="Icône Éditer">
							</a>
							<a href="index.php?access=adminblog&amp;action=delete&amp;post=<?= htmlspecialchars($res_post_list['id']);?>" class="icon"  <?= $refDelete?> >
								<img src="public/images/delete.png" alt="Icône Supprimer">
							</a>
							<p><strong><?= $post_nb.' - '.htmlspecialchars($res_post_list['title']); ?></strong>
							</p>
						</div>
					</li>
			  	<?php }
			  	else { ?>
					<li>
						<div id="tocEntry">
							<a href="index.php?access=adminblog&amp;post=<?= htmlspecialchars($res_post_list['id']);?>" class="icon" <?php if (isset($creation)) {echo $refRead;} ?>title="Lire">
								<img src="public/images/read.png" alt="Icône Lire">
							</a>
							<a href="index.php?access=adminblog&amp;action=edit&amp;post=<?= htmlspecialchars($res_post_list['id']);?>" class="icon" title="Éditer">
								<img src="public/images/edit.png" alt="Icône Éditer">
							</a>
							<a href="index.php?access=adminblog&amp;action=delete&amp;post=<?= htmlspecialchars($res_post_list['id']);?>" class="icon" title="Supprimer">
								<img src="public/images/delete.png" alt="Icône Supprimer">
							</a>
							<p>
								<a href="index.php?access=adminblog&amp;post=<?= htmlspecialchars($res_post_list['id']);?>" <?php if (isset($refId)) {echo 'id = "'.$post_nb.'" ';} ?> ><?= $post_nb.' - '.htmlspecialchars($res_post_list['title']); ?>
								</a>
							</p>
						</div>
					</li>
			  	<?php }
			  	$post_nb++;

			  }
		  	if (isset($new_chapter_location)) { ?>
	  			<li><strong><?= $post_nb ?> - ICI, LE NOUVEAU CHAPITRE</strong></li>
	  		<?php }	?>
			 </ul>

			<h3>Commentaires</h3>
			<ul>
				<li>
				<a href="index.php?access=adminblog&amp;action=moderation" <?= $refModeration ?> title="Modération (M)">Modération</a>
				</li>
			</ul>
		</aside>