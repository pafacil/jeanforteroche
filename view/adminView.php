<?php
$title = 'Administration - Billet simple pour l\'Alaska'; 
$head_options = '';

ob_start(); ?>
    <section id="book">
	  	<div id="chapter">
	  		<div id="chapterTitle">
				<?php if ($prev_id != 0) { ?>
					<div id="prev"><a href="index.php?access=adminblog&amp;post=<?= $prev_id ?>" id="leftArrow" class="fas fa-angle-left"></a></div>
				<?php } ?>
				<h3 id="title">
                <?= htmlspecialchars($myPost['title']); ?>
				</h3>
				<?php if ($next_id != 0) { ?>
					<div id ="next"><a href="index.php?access=adminblog&amp;post=<?= $next_id ?>" id="rightArrow" class="fas fa-angle-right"></a></div>
				<?php } ?>
            </div>			  		
			<p><?= html_entity_decode($myPost['content']); ?></p>
		</div>

		<div id="comments">
			<h4 id="theComments" title="Commentaires (C)">Commentaires <?= '('.$nb_comments.')'; ?>
			 
				<span id="down" <?php if ($nb_comments > 0) { ?>class="fas fa-caret-down"><?php } ?></span> 
			</h4>
			<div id="activeComments" class="hidden">
				<?php 
				 while ($res_comments = $req_comments -> fetch()) { 
				  	echo '<div class="myComment"><p><strong>'.htmlspecialchars($res_comments['author']).'</strong> a posté le '.$res_comments['date_creation'].'</p><p class=myCommentContent>'.nl2br(htmlspecialchars($res_comments['content'])).'</p> ';
				  	if ($res_comments['status'] == 2) {
				  		echo '<p><strong>Vérifié</strong></div>';
				    } else {
				    	if ($res_comments['warning'] == 0) {
				    		echo '<p><strong>Non signalé</strong>';
				    	} else {
				    		echo '<p><strong>Signalé '.$res_comments['warning'].' fois</strong>';
				    	}
				    	echo '<a href="index.php?access=adminblog&amp;action=commentBanned&amp;comment='.htmlspecialchars($res_comments['id']).'" class="icon"><img src="public/images/disable.png" alt="Désactiver"></a><a href="index.php?access=adminblog&amp;action=commentChecked&amp;comment='.htmlspecialchars($res_comments['id']).'" class="icon"><img src="public/images/enable.png" alt="Désactiver"></a></p></div>';} 
				} ?>
			</div>
		</div>
	</section>

<?php $content = ob_get_clean();

require('template_admin.php'); ?>