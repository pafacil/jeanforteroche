<!DOCTYPE html>
<html lang="fr">
	<head>
		<meta charset="UTF-8">
	    <title><?= $title ?></title>
	    <link href="public/css/style.css" rel="stylesheet" /> 
	    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet"> 
	    <?= $head_options ?>
	</head>
	<body class="admin">

		<span class="scrollBtn"></span>

		<header>
			<div id="headerLogo"><img src="public/images/typewriter.png" alt="Logo livre"></div>
			<div id="headerContent">
				<h1>Billet <span class="special">simple</span> pour l'Alaska</h1>
				<h2>Administration du blog</h2>
				<form action="index.php?action=deconnexion" method="post" enctype="multipart/form-data" id="deconnexion">
					<input type="submit" class="deconnexion" value="Déconnexion"/>	
				</form>
				<div id="helpRequired">Aide</div>
			</div>
		</header>

	    <?= $content ?>
		<?php require('view/tocView.php'); ?>

        <article id="help">
        	<div id="closeHelp" class="fas fa-times-circle"></div>
        	<h1>Aide</h1>
        	<img src="public/images/help.png" alt="Aide">
        	<h2>Navigation</h2><hr>
        	<p>Pour naviguer à travers les chapitres, vous pouvez utiliser les flèches ou l'index.<br/> Vous pouvez également naviguer au clavier (flèches droite et gauche, ainsi que directement en tapant le numéro d'un chapitre). La flèche "haut" amène au mode "Création" d'un nouveau chapitre.</p>        	
        	<h2>Raccourcis supplémentaires</h2><hr>
        	<p>En mode "lecture", vous pouvez utilisez E pour éditer le chapitre en cours de lecture, S pour le supprimer. </p>
        	<p><strong>Attention</strong> : en mode "Édition" ou "Création", seul la flèche "haut" (création) et "bas" (retour au mode lecture) sont actifs.</p>
        	<h2>Modération des commentaires</h2><hr>
        	<p>Accessibles depuis chaque chapitre ou depuis la page "Modération" (où sont répertoriés uniquement les commentaires signalés), un clic "pouce vert" valide définitivement un commentaire, un clic "pouce rouge" le sanctionne irrémédiablement.</p>
        </article>    

		<footer>
			<div id="footerContent">
	            <strong>© Copyright - Tous droits réservés - 2018</strong>
	            <div id="footerLogo"><img src="public/images/pen.png" alt="Logo encrier"></div>
	        </div>
		</footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="public/js/main_admin.js"></script>                                            
	</body>
</html>