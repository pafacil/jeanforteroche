<?php
$title = 'Administration - Billet simple pour l\'Alaska'; 
$head_options = ''; 

ob_start(); ?>
    <section id="commentSection">
		<div id="comments">
			<?php /*$res_comments = $req_comments -> fetch();*/
			$isComment = $req_comments -> rowCount();
			if ($isComment > 0) { ?>
			<h3>Commentaires signalés (<?= $isComment ?>) </h3>
		<?php 
		 while ($res_comments = $req_comments -> fetch()) { 
		  	echo '<div class="myComment"><p><strong>'.htmlspecialchars($res_comments['author']).'</strong> a posté le '.$res_comments['date_creation'].', sur <strong>"'.htmlspecialchars($res_comments['title']).'"</strong></p><p class=myCommentContent>'.nl2br(htmlspecialchars($res_comments['content'])).'</p> ';
		  	if ($res_comments['status'] == 2) {
		  		echo '<p><strong>Vérifié</strong></div>';
		    } else {
		    	if ($res_comments['warning'] == 0) {
		    		echo '<p><strong>Non signalé</strong>';
		    	} else {
		    		echo '<p><strong>Signalé '.$res_comments['warning'].' fois</strong>';
		    	}
		    	echo '<a href="index.php?access=adminblog&amp;action=commentBanned&amp;comment='.htmlspecialchars($res_comments['id']).'" class="icon"><img src="public/images/disable.png" alt="Désactiver"></a><a href="index.php?access=adminblog&amp;action=commentChecked&amp;comment='.htmlspecialchars($res_comments['id']).'" class="icon"><img src="public/images/enable.png" alt="Désactiver"></a></p></div>';} 
			} 
				 }
			else { ?> 
			<h3>Aucun commentaire signalé. </h3>
			<?php } ?>
		</div>
	</section>

<?php $content = ob_get_clean();

require('template_admin.php'); ?>