<?php 
// Chargement des classes
require_once('model/PostManager.php');
require_once('model/CommentManager.php');
require_once('model/UserManager.php');

function publicBlog($post_id) {
	// In case of first visit, we display the explanation page (just once)
	// if (!isset($_COOKIE['notFirstTime'])) {
	if (true) {
		setcookie('notFirstTime', 'true', time() + 365*24*3600, null, null, false, true);
		require('view/firstTimeView.php');
	} else {
	    $postManager = new \JeanForteroche\Model\PostManager();
	    $commentManager = new \JeanForteroche\Model\CommentManager();

	    $res_post = $postManager -> getPost($post_id);

		if (!$res_post) {
			$res_post = $postManager -> getLastPost();	
			$post_id = $res_post['id'];	
		}		

		$nb_comments = $commentManager -> getNbCommentPerPost($post_id);
		$req_comments = $commentManager -> getComments($post_id,'');
		$req_posts_list = $postManager -> getPostsList();

		$ranking = rankPost($post_id);
		$prev_id = $ranking['prev_id'];
		$next_id = $ranking['next_id'];

		require('view/chapterView.php');
	}
}

function adminBlog($post_id) {	
    $postManager = new \JeanForteroche\Model\PostManager();
    $commentManager = new \JeanForteroche\Model\CommentManager();
    $myPost = $postManager -> getPost($post_id);

	if (!$myPost) {
		$myPost = $postManager -> getLastPost();	
		$post_id = $myPost['id'];	
	}

	$rank = rankPost($post_id);

	$prev_id = $rank['prev_id'];
	$next_id = $rank['next_id'];
	$new_chapter_to_create = 1;

	$nb_comments = $commentManager -> getNbCommentPerPost($post_id);
	$req_comments = $commentManager -> getComments($post_id,"warning");
	$req_posts_list = $postManager -> getPostsList();

	$refRead = ' title="Lire" ';
	$refEdit = ' id="editChapter" title="Éditer (E)" ';
	$refDelete = ' id="deleteChapter" title="Supprimer (S)" ';
	$refModeration = ' id="moderation" ';
	$refId = true;

	require('view/adminView.php');
}

function loginScreen() {
	$warningMessage = (isset($_GET['action']) && $_GET['action'] == "incorrectCredentials") ? '<p class="warning">Utilisateur ou mot de passe incorrect</p>' : '<p><em>* Les 2 champs sont obligatoires.</em></p>';

	require('view/loginView.php');
}

function login() {
    $userManager = new \JeanForteroche\Model\UserManager();

	$inputMail = (isset($_POST['userMail'])) ? htmlspecialchars($_POST['userMail']) : '';
	$password = (isset($_POST['password'])) ? htmlspecialchars($_POST['password']) : '';

	$isValidPassword = $userManager -> checkPassword($inputMail, $password);
	if ($isValidPassword) {
		$_SESSION['activeSession'] = 'valid';
		header('Location: index.php?access=adminblog');
		} 
	else {
		header('Location: index.php?access=adminblog&action=incorrectCredentials');
	}	
}

function rankPost($post_id) {
	// Provide needed variables for the prev-next links
    $postManager = new \JeanForteroche\Model\PostManager();
    $rank = [];

	$ranking = $postManager -> getRankedPost();

	$position_current_post = array_search($post_id,$ranking);
	$prev_rank = $position_current_post - 1;
	$next_rank = $position_current_post + 1;

	$rank['prev_id'] = (isset($ranking[$prev_rank])) ? $ranking[$prev_rank] : 0;
	$rank['next_id'] = (isset($ranking[$next_rank])) ? $ranking[$next_rank] : 0;
	
	return $rank;
}