<?php 
// Chargement des classes
require_once('model/PostManager.php');
require_once('model/CommentManager.php');
require_once('model/UserManager.php');

function addComment() {
    $commentManager = new \JeanForteroche\Model\CommentManager();
    $postManager = new \JeanForteroche\Model\PostManager();

	$post_id = htmlspecialchars($_POST['post_id']);
	$name = htmlspecialchars(ucwords($_POST['name']));
	$mail = htmlspecialchars($_POST['mail']);
	$message = htmlspecialchars(ucfirst($_POST['message']));

	$post = $postManager -> getPost($post_id);

	if ($post) {
		$commentManager -> insertComment($post_id, $name, $mail, $message);
	}

	header('Location: index.php?post='.$post_id);
}

function commentWarning($comment_id) {
    $commentManager = new \JeanForteroche\Model\CommentManager();
	$comment = $commentManager -> getComment($comment_id);

	if ($comment) {
		$post_id = $comment['post_id'];
		$commentManager -> insertCommentWarning($comment_id);
	}

	if (!isset($post_id)) {
		header('Location: index.php');
	} else {
		header('Location: index.php?post='.$post_id);
	}
}

function commentAction($comment_id) {
    $commentManager = new \JeanForteroche\Model\CommentManager();

	$comment = $commentManager -> getComment($comment_id);

	if ($comment) {
		$post_id = $comment['post_id'];
		if ($_GET['action'] == "commentBanned") {
			$commentManager -> actionComment($comment_id,"ban");
		} else if ($_GET['action'] == "commentChecked") {
			$commentManager -> actionComment($comment_id,"checked");			
		}
	}

	if (!isset($post_id)) {
		header('Location: index.php?access=adminblog');
	} else if (isset($_GET['section']) AND ($_GET['section']) == "moderation") {
		header('Location: index.php?access=adminblog&action=moderation');
	} else {
		header('Location: index.php?access=adminblog&post='.$post_id);
	}
}

function moderateComments() {
    $postManager = new \JeanForteroche\Model\PostManager();
	$req_posts_list = $postManager -> getPostsList();
	$commentManager = new \JeanForteroche\Model\CommentManager();
	$req_comments = $commentManager -> getComments('','');
	
    // Variables required in the TOC view
    $post_id = 0;
	$new_chapter_to_create = 1;
	$refModeration = '';

	// For keyboard navigation
	$refRead = '';
	$refEdit = ' title="Éditer" ';
	$refDelete = ' title="Supprimer" ';
	$refModeration = '';
	$refId = true;

	require('view/commentsView.php');	
}
