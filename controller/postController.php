<?php 
// Chargement des classes
require_once('model/PostManager.php');
require_once('model/CommentManager.php');
require_once('model/UserManager.php');

function createPost() {
    $postManager = new \JeanForteroche\Model\PostManager();
	$req_posts_list = $postManager -> getPostsList();

	$new_chapter_location = 1;
	$post_id = 0;

	$ranking = rankPost($post_id);
	$prev_id = $ranking['prev_id'];
	$next_id = 0;	

	// For keyboard navigation
	$refPrev = "index.php?access=adminblog&action=edit&post=".$prev_id;
	$refNext = '';
	$refAction = "index.php?access=adminblog&action=addPost";
	$refTitle = 'placeholder="...mon titre..."';
	$refContent = '';
	$refRead = ' id="readChapter" title="Lire (Flèche Bas)" ';
	$refEdit = ' title="Éditer" ';
	$refDelete = ' title="Supprimer" ';
	$refModeration = '';
	$creation = 1;

	require('view/editionView.php');
}

function addPost() {
    $postManager = new \JeanForteroche\Model\PostManager();

	$title = trim(htmlspecialchars(ucfirst($_POST['title'])));
	$post = trim(htmlspecialchars($_POST['post']));

	if ($post && $title) {
		$postManager -> insertPost($title, $post);
	} else {
		echo "Un des champs est vide";
	}

	header('Location: index.php?access=adminblog');
}

function editPost($post_id) {
    $postManager = new \JeanForteroche\Model\PostManager();
	$req_posts_list = $postManager -> getPostsList();

	$post = $postManager -> getPost($post_id);
	if ($post) {
		$chapter_title = $post['title'];
		$chapter_content = $post['content'];
	}

	$ranking = rankPost($post_id);
	$prev_id = $ranking['prev_id'];
	$next_id = $ranking['next_id'];	

	$new_chapter_to_create = 1;

	// For keyboard navigation
	$refPrev = "index.php?access=adminblog&action=edit&post=".$prev_id;
	$refNext = "index.php?access=adminblog&action=edit&post=".$next_id;
	$refAction = "index.php?access=adminblog&action=updatePost&post=".$post_id;
	$refTitle = 'value="'.$chapter_title.'"';
	$refContent = $chapter_content;
	$refRead = ' id="readChapter" title="Lire (Flèche Bas)" ';
	$refEdit = ' title="Éditer" ';
	$refDelete = ' title="Supprimer" ';
	$refModeration = '';

	require('view/editionView.php');
}

function updatePost($post_id) {
    $postManager = new \JeanForteroche\Model\PostManager();

	$title = htmlspecialchars(ucfirst($_POST['title']));
	$post = htmlspecialchars($_POST['post']);

	if ($post && $title) {
		$postManager -> updatePost($post_id, $title, $post);
	}

	header('Location: index.php?access=adminblog&post='.$post_id);
}

function deletePost($post_id) {
    $postManager = new \JeanForteroche\Model\PostManager();
    $commentManager = new \JeanForteroche\Model\CommentManager();

	$post = $postManager -> getPost($post_id);

	if ($post) {
		$commentManager -> deleteComments($post_id);
		$postManager -> deletePost($post_id);
	}

	header('Location: index.php?access=adminblog');
}